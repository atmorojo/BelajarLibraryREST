package gulajava.teslibraryrests.internet.konverters;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class JacksonObjectMapper {

    private ObjectMapper mObjectMapper;

    private static JacksonObjectMapper ourInstance;

    public static JacksonObjectMapper getInstance() {

        if (ourInstance == null) {
            ourInstance = new JacksonObjectMapper();
        }

        return ourInstance;
    }

    private JacksonObjectMapper() {

        mObjectMapper = new ObjectMapper();
        mObjectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        mObjectMapper.configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, false);
        mObjectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);

        setObjectMapper(mObjectMapper);
    }

    public ObjectMapper getObjectMapper() {
        return mObjectMapper;
    }

    public void setObjectMapper(ObjectMapper objectMapper) {
        mObjectMapper = objectMapper;
    }
}
