package gulajava.teslibraryrests.internet.loopje;

import gulajava.teslibraryrests.internet.KonstanInternet;

/**
 * Created by Gulajava Ministudio on 1/25/16.
 */
public class ApisLoopj {


    /**
     * API UNTUK DAFTAR KANDIDAT
     **/
    //http://api.pemiluapi.org/calonpilkada/api/candidates?apiKey=8e67d2396454f98c370596b139194015&limit=100
    public static String getKandidatDaftar(String limit) {

        return KonstanInternet.ALAMATSERVER + "/calonpilkada/api/candidates?apiKey=" +
                KonstanInternet.APIKEYS + "&limit=" + limit;
    }

}
