package gulajava.teslibraryrests.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import gulajava.teslibraryrests.modelan.Candidates;
import gulajava.teslibraryrests.modelan.Paslon;
import gulajava.teslibraryrests.modelan.Provinsi;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class ListKandidatAdapter extends ArrayAdapter<Candidates> {


    private Context mContext;
    private List<Candidates> mCandidates;
    private int restId;

    public ListKandidatAdapter(Context context, int resource, List<Candidates> candidates) {
        super(context, resource, candidates);
        mContext = context;
        mCandidates = candidates;
        restId = resource;
    }


    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if (convertView == null) {

            convertView = LayoutInflater.from(mContext).inflate(restId, parent, false);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Candidates candidates = mCandidates.get(position);
        Provinsi provinsi = candidates.getProvinsi();
        String stringprovinsi = provinsi.getNama();

        List<Paslon> paslon = candidates.getPaslon();
        Paslon paslon1 = paslon.get(0);
        String namapaslon1 = paslon1.getNama();
        Paslon paslon2 = paslon.get(1);
        String namapaslon2 = paslon2.getNama();

        viewHolder.getTextViewKandidat1().setText(namapaslon1);
        viewHolder.getTextViewKandidat2().setText(namapaslon2);
        viewHolder.getTextViewKandidatDaerah().setText(stringprovinsi);

        return convertView;
    }


}
