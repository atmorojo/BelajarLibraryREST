package gulajava.teslibraryrests.adapter;

import android.view.View;
import android.widget.TextView;

import gulajava.teslibraryrests.R;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class ViewHolder {


    private View mView;

    private TextView mTextViewKandidat1 = null;
    private TextView mTextViewKandidat2 = null;
    private TextView mTextViewKandidatDaerah = null;

    public ViewHolder(View view) {
        mView = view;
    }


    public TextView getTextViewKandidat1() {

        if (mTextViewKandidat1 == null) {
            mTextViewKandidat1 = (TextView) mView.findViewById(R.id.teks_namakandidat1);
        }
        return mTextViewKandidat1;
    }

    public TextView getTextViewKandidat2() {

        if (mTextViewKandidat2 == null) {
            mTextViewKandidat2 = (TextView) mView.findViewById(R.id.teks_namakandidat2);
        }
        return mTextViewKandidat2;
    }

    public TextView getTextViewKandidatDaerah() {

        if (mTextViewKandidatDaerah == null) {
            mTextViewKandidatDaerah = (TextView) mView.findViewById(R.id.teks_daerahkandidat);
        }
        return mTextViewKandidatDaerah;
    }
}
