package gulajava.teslibraryrests.aktivitas;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.afollestad.bridge.Bridge;
import com.afollestad.bridge.BridgeException;
import com.afollestad.bridge.ResponseConvertCallback;
import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.google.gson.reflect.TypeToken;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.TextHttpResponseHandler;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;

import bolts.Continuation;
import bolts.Task;
import cz.msebera.android.httpclient.Header;
import gulajava.teslibraryrests.R;
import gulajava.teslibraryrests.adapter.ListKandidatAdapter;
import gulajava.teslibraryrests.internet.CekInternet;
import gulajava.teslibraryrests.internet.KonstanInternet;
import gulajava.teslibraryrests.internet.bridgeshttp.BridgeSingleton;
import gulajava.teslibraryrests.internet.ionkoush.ApisION;
import gulajava.teslibraryrests.internet.konverters.OkHttpConverter;
import gulajava.teslibraryrests.internet.loopje.ApisLoopj;
import gulajava.teslibraryrests.internet.loopje.LoopJSingleton;
import gulajava.teslibraryrests.internet.okhttps.OkHttpNets;
import gulajava.teslibraryrests.internet.retrofits1.Apis;
import gulajava.teslibraryrests.internet.retrofits1.RestAdapterSingleton;
import gulajava.teslibraryrests.internet.volleys.ApisVolley;
import gulajava.teslibraryrests.internet.volleys.JacksonRequest;
import gulajava.teslibraryrests.internet.volleys.Volleys;
import gulajava.teslibraryrests.modelan.Candidates;
import gulajava.teslibraryrests.modelan.DataResults;
import gulajava.teslibraryrests.modelan.KandidatModel;
import gulajava.teslibraryrests.modelan.Results;
import okhttp3.Call;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
public class HalamanUtama extends AppCompatActivity {

    private Toolbar mToolbar;
    private ActionBar mActionBar;
    private ListView mListView;
    private EditText mEditText;

    private Spinner mSpinner;
    private ArrayAdapter<String> mStringArrayAdapter;
    private int pilihan = 0;
    private String[] arraystr;

    private boolean isJalan = false;

    private long waktumulai = 0;
    private long waktuakhir = 0;
    private long waktuselisih = 0;
    private Calendar mCalendar;

    //retrofit 1.x
    private Apis mApis;
    private List<Candidates> mCandidatesList;

    private Call mCall;

    private ProgressDialog mProgressDialog;

    private String LIMIT_DATA_DEF = "100";
    private String LIMIT_DATA = "100";

    private AsyncHttpClient mAsyncHttpClient;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_utama);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        if (mToolbar != null) {
            HalamanUtama.this.setSupportActionBar(mToolbar);
        }

        mActionBar = HalamanUtama.this.getSupportActionBar();
        assert mActionBar != null;
        mActionBar.setTitle(R.string.app_name);

        mCandidatesList = new ArrayList<>();
        inisialisasi();

    }

    @Override
    protected void onResume() {
        super.onResume();
        isJalan = true;

        try {
            Bridge.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        isJalan = false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        HalamanUtama.this.getMenuInflater().inflate(R.menu.menu_main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        sembunyikeyboard(HalamanUtama.this, mListView);

        switch (item.getItemId()) {

            case R.id.action_segar:

                CekInternet cekInternet = new CekInternet(HalamanUtama.this);
                boolean isInternet = cekInternet.cekStatusInternet();

                LIMIT_DATA = mEditText.getText().toString();
                int intlimit;
                if (LIMIT_DATA.length() > 0) {
                    intlimit = Integer.valueOf(LIMIT_DATA);
                } else {
                    intlimit = 100;
                    LIMIT_DATA = LIMIT_DATA_DEF;
                    mEditText.setText(LIMIT_DATA_DEF);
                }

                if (isInternet) {

                    if (intlimit > 0 && intlimit < 801) {
                        cekPilihan();
                    } else {
                        Toast.makeText(HalamanUtama.this, "Data kurang atau datanya kebanyakan", Toast.LENGTH_SHORT).show();
                    }

                } else {
                    Toast.makeText(HalamanUtama.this, "Internet ga nyambung", Toast.LENGTH_SHORT).show();
                }

                return true;

            case R.id.action_buangdaftar:

                mCandidatesList = new ArrayList<>();
                setelListView();
                return true;

        }

        return super.onOptionsItemSelected(item);
    }

    private void inisialisasi() {

        mSpinner = (Spinner) findViewById(R.id.spinnertipe);
        arraystr = HalamanUtama.this.getResources().getStringArray(R.array.daftar_libs);
        mStringArrayAdapter = new ArrayAdapter<>(HalamanUtama.this, android.R.layout.simple_spinner_item, arraystr);
        mStringArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        mSpinner.setAdapter(mStringArrayAdapter);
        mSpinner.setOnItemSelectedListener(mOnItemSelectedListener);

        mListView = (ListView) findViewById(R.id.list);

        mEditText = (EditText) findViewById(R.id.edit_data);
        mEditText.setText(LIMIT_DATA_DEF);
    }


    AdapterView.OnItemSelectedListener mOnItemSelectedListener = new AdapterView.OnItemSelectedListener() {
        @Override
        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            pilihan = i;
            String judulibs = arraystr[i];
            mActionBar.setSubtitle(judulibs);

        }

        @Override
        public void onNothingSelected(AdapterView<?> adapterView) {

        }
    };


    private void cekPilihan() {

        switch (pilihan) {

            case 0:
                //volley 1.0.19
                ambilVolleyRequestKandidat();
                break;

            case 1:
                //retrofit 1.9.0
                ambilDataRetrofit1();
                break;

            case 2:
                //OkHttp 3.0.1
                ambilOkHttpAsync();
                break;

            case 3:
                //bridge 3.x
                ambilDataBridgeKandidat();
                break;

            case 4:
                //loopj
                ambilDataLoopJ();
                break;

            case 5:
                //ION
                ambilKandidatION();
                break;
        }

    }


    //HITUNG WAKTU AMBIL DATA REST
    private void hitungWaktuAkhir() {

        try {
            waktuselisih = waktuakhir - waktumulai;
            double doselisih = (Double.valueOf("" + waktuselisih)) / 1000;

            Snackbar.make(mListView, "Waktu terima data " + doselisih + " detik", Snackbar.LENGTH_INDEFINITE)
                    .setAction("OK", mOnClickListenerSnack).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    View.OnClickListener mOnClickListenerSnack = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

        }
    };


    //SETEL KE LISTVIEW
    private void setelListView() {

        ListKandidatAdapter listKandidatAdapter = new ListKandidatAdapter(HalamanUtama.this, R.layout.desain_baris, mCandidatesList);
        mListView.setAdapter(listKandidatAdapter);

    }


    //PROGRESS DIALOG
    private void tampilDialogProgress() {

        mProgressDialog = new ProgressDialog(HalamanUtama.this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("ambil data...");
        mProgressDialog.show();

    }


    /**
     * RETROFIT 1.9.0 AMBIL API
     * http://square.github.io/retrofit/
     **/

    private void ambilDataRetrofit1() {

        tampilDialogProgress();

        mApis = RestAdapterSingleton.getInstance().getApis();

        mCalendar = Calendar.getInstance();
        waktumulai = mCalendar.getTimeInMillis();

        mApis.getDaftarKandidat(KonstanInternet.APIKEYS, LIMIT_DATA, new Callback<KandidatModel>() {
            @Override
            public void success(KandidatModel kandidatModel, Response response) {

                mCalendar = Calendar.getInstance();
                waktuakhir = mCalendar.getTimeInMillis();

                if (isJalan) {


                    DataResults dataResults = kandidatModel.getData();
                    Results results = dataResults.getResults();
                    mCandidatesList = results.getCandidates();

                    //hitung waktu dalam ms
                    hitungWaktuAkhir();

                    setelListView();

                    mProgressDialog.dismiss();
                }
            }

            @Override
            public void failure(RetrofitError error) {

                if (isJalan) {
                    Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();
                    mCandidatesList = new ArrayList<>();
                    setelListView();
                    mProgressDialog.dismiss();
                    error.printStackTrace();
                }
            }
        });
    }


    /**
     * AMBIL DENGAN METODE VOLLEY + OKHTTP
     * https://github.com/mcxiaoke/android-volley
     * http://developer.android.com/training/volley/index.html
     **/

    private void ambilVolleyRequestKandidat() {

        tampilDialogProgress();

        String urls = ApisVolley.getLinkCalonPilkada(LIMIT_DATA);
        Map<String, String> headers = new HashMap<>();
        Map<String, String> parameters = new HashMap<>();

        JacksonRequest<KandidatModel> jacksonRequest = ApisVolley.getKandidatRequest(
                Request.Method.GET,
                urls, headers, parameters, "",
                new com.android.volley.Response.Listener<KandidatModel>() {
                    @Override
                    public void onResponse(KandidatModel response) {

                        mCalendar = Calendar.getInstance();
                        waktuakhir = mCalendar.getTimeInMillis();

                        if (isJalan) {


                            DataResults dataResults = response.getData();
                            Results results = dataResults.getResults();
                            mCandidatesList = results.getCandidates();

                            //hitung waktu dalam ms
                            hitungWaktuAkhir();

                            setelListView();

                            mProgressDialog.dismiss();
                        }

                    }
                },
                new com.android.volley.Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {

                        if (isJalan) {
                            Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();
                            mCandidatesList = new ArrayList<>();
                            setelListView();
                            mProgressDialog.dismiss();
                            error.printStackTrace();
                        }
                    }
                }
        );

        mCalendar = Calendar.getInstance();
        waktumulai = mCalendar.getTimeInMillis();
        Volleys.getInstance(HalamanUtama.this).addToRequestQueue(jacksonRequest);
    }


    /**
     * AMBIL DATA DENGAN OKHTTP + BOLT TASK
     * https://github.com/square/okhttp
     * https://github.com/BoltsFramework/Bolts-Android
     **/

    private void ambilOkHttpAsync() {

        tampilDialogProgress();
        mCall = OkHttpNets.getInstance().getRequestKandidatPartai(LIMIT_DATA);

        mCalendar = Calendar.getInstance();
        waktumulai = mCalendar.getTimeInMillis();

        mCall.enqueue(new okhttp3.Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

                e.printStackTrace();

                if (isJalan) {

                    Task.call(new Callable<Object>() {
                        @Override
                        public Object call() throws Exception {

                            mCandidatesList = new ArrayList<>();
                            setelListView();
                            mProgressDialog.dismiss();
                            Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();

                            return null;
                        }
                    }, Task.UI_THREAD_EXECUTOR);

                }
            }

            @Override
            public void onResponse(Call call, final okhttp3.Response response) throws IOException {

                mCalendar = Calendar.getInstance();
                waktuakhir = mCalendar.getTimeInMillis();

                if (isJalan && response.isSuccessful()) {

                    Task.callInBackground(new Callable<List<Candidates>>() {
                        @Override
                        public List<Candidates> call() throws Exception {

                            return OkHttpConverter.getCandidatParse(response);
                        }
                    })
                            .continueWith(new Continuation<List<Candidates>, Object>() {
                                @Override
                                public Object then(Task<List<Candidates>> task) throws Exception {

                                    mCandidatesList = task.getResult();
                                    //hitung waktu dalam ms
                                    hitungWaktuAkhir();
                                    setelListView();
                                    mProgressDialog.dismiss();

                                    return null;
                                }
                            }, Task.UI_THREAD_EXECUTOR);


                } else {
                    Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();
                    mCandidatesList = new ArrayList<>();
                    setelListView();
                    mProgressDialog.dismiss();
                }
            }
        });
    }


    /**
     * AMBIL DATA DENGAN LOOPJ
     * http://loopj.com/android-async-http/
     **/

    private void ambilDataLoopJ() {

        tampilDialogProgress();

        String urls = ApisLoopj.getKandidatDaftar(LIMIT_DATA);
        mAsyncHttpClient = LoopJSingleton.getInstance().getAsyncHttpClient();

        mCalendar = Calendar.getInstance();
        waktumulai = mCalendar.getTimeInMillis();

        mAsyncHttpClient.get(urls, new TextHttpResponseHandler() {
            @Override
            public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {

                if (isJalan) {
                    Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();
                    mCandidatesList = new ArrayList<>();
                    setelListView();
                    mProgressDialog.dismiss();
                }

            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, final String responseString) {

                mCalendar = Calendar.getInstance();
                waktuakhir = mCalendar.getTimeInMillis();

                if (isJalan) {

                    Task.callInBackground(new Callable<List<Candidates>>() {
                        @Override
                        public List<Candidates> call() throws Exception {

                            return OkHttpConverter.getCandidatParseString(responseString);
                        }
                    })
                            .continueWith(new Continuation<List<Candidates>, Object>() {
                                @Override
                                public Object then(Task<List<Candidates>> task) throws Exception {

                                    mCandidatesList = task.getResult();
                                    //hitung waktu dalam ms
                                    hitungWaktuAkhir();
                                    setelListView();
                                    mProgressDialog.dismiss();

                                    return null;
                                }
                            }, Task.UI_THREAD_EXECUTOR);
                }

            }
        });

    }


    /**
     * AMBIL DATA DENGAN ION KOUSH
     * https://github.com/koush/ion
     **/

    private void ambilKandidatION() {

        tampilDialogProgress();

        String urls = ApisION.getKandidatDaftar(LIMIT_DATA);

        mCalendar = Calendar.getInstance();
        waktumulai = mCalendar.getTimeInMillis();

        Future<KandidatModel> futureKandidat = Ion.with(HalamanUtama.this)
                .load(urls)
                .setTimeout(10000)
                .setHeader("Content-Type", "application/json")
                .as(new TypeToken<KandidatModel>() {
                })
                .setCallback(new FutureCallback<KandidatModel>() {
                    @Override
                    public void onCompleted(Exception e, KandidatModel result) {

                        mCalendar = Calendar.getInstance();
                        waktuakhir = mCalendar.getTimeInMillis();

                        if (isJalan) {

                            if (result != null) {


                                DataResults dataResults = result.getData();
                                Results results = dataResults.getResults();
                                mCandidatesList = results.getCandidates();

                                //hitung waktu dalam ms
                                hitungWaktuAkhir();

                                setelListView();

                            } else {
                                Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();
                                mCandidatesList = new ArrayList<>();
                                setelListView();

                                e.printStackTrace();
                            }

                            mProgressDialog.dismiss();
                        }

                    }
                });
    }


    /**
     * AMBIL DATA DENGAN LIB BRIDGE
     * http://afollestad.github.io/bridge/
     **/

    private void ambilDataBridgeKandidat() {

        tampilDialogProgress();

        String urls = BridgeSingleton.getInstance().getKandidatDaftar(LIMIT_DATA);

        mCalendar = Calendar.getInstance();
        waktumulai = mCalendar.getTimeInMillis();

        Bridge.get(urls)
                .asString(new ResponseConvertCallback<String>() {
                    @Override
                    public void onResponse(@NonNull com.afollestad.bridge.Response response, final String s, BridgeException e) {

                        mCalendar = Calendar.getInstance();
                        waktuakhir = mCalendar.getTimeInMillis();

                        if (isJalan) {

                            if (s != null) {

                                Task.callInBackground(new Callable<List<Candidates>>() {
                                    @Override
                                    public List<Candidates> call() throws Exception {

                                        return OkHttpConverter.getCandidatParseString(s);
                                    }
                                })
                                        .continueWith(new Continuation<List<Candidates>, Object>() {
                                            @Override
                                            public Object then(Task<List<Candidates>> task) throws Exception {

                                                mCandidatesList = task.getResult();
                                                //hitung waktu dalam ms
                                                hitungWaktuAkhir();
                                                setelListView();
                                                mProgressDialog.dismiss();

                                                return null;
                                            }
                                        }, Task.UI_THREAD_EXECUTOR);
                            } else {
                                mProgressDialog.dismiss();
                                Toast.makeText(HalamanUtama.this, R.string.toastgagal, Toast.LENGTH_SHORT).show();
                                mCandidatesList = new ArrayList<>();
                                setelListView();
                                e.printStackTrace();
                            }
                        }

                    }
                });
    }


    public static void sembunyikeyboard(Context context, View view) {
        InputMethodManager manager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        manager.hideSoftInputFromWindow(view.getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);
    }


}
