package gulajava.teslibraryrests.modelan;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Gulajava Ministudio on 2/1/16.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Candidates {

    private Provinsi provinsi;
    private List<Paslon> paslon = new ArrayList<>();

    public Candidates() {
    }

    public Provinsi getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(Provinsi provinsi) {
        this.provinsi = provinsi;
    }

    public List<Paslon> getPaslon() {
        return paslon;
    }

    public void setPaslon(List<Paslon> paslon) {
        this.paslon = paslon;
    }
}
