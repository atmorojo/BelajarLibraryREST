# Library Rests

Aplikasi untuk ujicoba dan latihan beberapa Library untuk transfer data via internet. Seperti :

  - [Volley]
  - [OkHttp]
  - [Retrofit]
  - [Bridge]
  - [LoopJ]
  - [Ion]
  
Aplikasi dan screenshoot dari kode sumber ini dapat dilihat di [Google Drives] berikut.

  

[Volley]:<http://developer.android.com/training/volley/index.html>
[OkHttp]:<https://github.com/square/okhttp>
[Retrofit]:<http://square.github.io/retrofit>
[LoopJ]:<http://loopj.com/android-async-http>
[Bridge]:<http://afollestad.github.io/bridge>
[Ion]:<https://github.com/koush/ion>
[Google Drives]:<https://drive.google.com/folderview?id=0ByL4wDgfTHp9cWpVbVU5Q2t2ODA&usp=sharing>